#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

for file in "$DIR"/../files/*.desktop ; do
    [ -x "$file" ] || continue
    cp "$file" $HOME/Desktop/.
done
